A way to play wfmu streams and archives in a linux terminal. Needs fzf and mpv to work. It has next to no original code. See the script for links to the original maker radio AB9IL who volunteered up his script when he saw my initial go at it. And thanks to WFMU.

The dead links in the archives will be nixed over time.

To use, clone or download it, place wfmufzfplayer in your $PATH and make it executable. Then drop the wfmuplayer folder in home/user/.cache/..